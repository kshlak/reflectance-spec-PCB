EESchema Schematic File Version 4
LIBS:soil-respiration-beta-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L soil-respiration-beta-rescue:Conn_01x04_Female-Connector J1
U 1 1 5D6DA2E0
P 3625 3025
F 0 "J1" V 3725 2950 50  0000 L CNN
F 1 "K30 co2 sensor UART Conn" V 3725 1825 50  0000 L CNN
F 2 "multispeq:K30_co2_sensor" H 3625 3025 50  0001 C CNN
F 3 "~" H 3625 3025 50  0001 C CNN
	1    3625 3025
	-1   0    0    -1  
$EndComp
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0101
U 1 1 5D6E5D8C
P 2650 6850
F 0 "#PWR0101" H 2650 6600 50  0001 C CNN
F 1 "GND" H 2655 6677 50  0000 C CNN
F 2 "" H 2650 6850 50  0001 C CNN
F 3 "" H 2650 6850 50  0001 C CNN
	1    2650 6850
	1    0    0    -1  
$EndComp
$Comp
L multispec:SparkFunMotorDriver_Dual_TB6612FNG U3
U 1 1 6096F22B
P 7650 5725
F 0 "U3" H 7625 6100 60  0000 C CNN
F 1 "SparkFunMotorDriver_Dual_TB6612FNG" H 7500 5175 60  0000 C CNN
F 2 "multispeq:SparkFunMotorDriver_Dual_TB6612FNG" H 7525 5275 60  0000 C CNN
F 3 "" H 7650 5725 60  0000 C CNN
	1    7650 5725
	-1   0    0    -1  
$EndComp
$Comp
L multispec:SparkFunMotorDriver_Dual_TB6612FNG U4
U 1 1 6096F279
P 7650 4125
F 0 "U4" H 7675 4475 60  0000 C CNN
F 1 "SparkFunMotorDriver_Dual_TB6612FNG" H 7575 5325 60  0000 C CNN
F 2 "multispeq:SparkFunMotorDriver_Dual_TB6612FNG" H 7575 5425 60  0000 C CNN
F 3 "" H 7650 4125 60  0000 C CNN
	1    7650 4125
	-1   0    0    -1  
$EndComp
$Comp
L multispec:SparkFun_Qwiik_Scale_PCB U5
U 1 1 6096FFAD
P 9775 3025
F 0 "U5" H 9775 3200 60  0000 C CNN
F 1 "SparkFun_Qwiik_Scale_PCB" H 8925 4575 60  0000 C CNN
F 2 "multispeq:SparkFun_Qwiic_Scale" H 9125 4350 60  0000 C CNN
F 3 "" H 9775 3025 60  0000 C CNN
	1    9775 3025
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_bipolar M1
U 1 1 60970858
P 1500 2225
F 0 "M1" H 1688 2348 50  0000 L CNN
F 1 "Stepper_Motor_bipolar" H 1688 2257 50  0000 L CNN
F 2 "TerminalBlock_4UCON:TerminalBlock_4UCON_19963_04x3.5mm_Straight" H 1510 2215 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 1510 2215 50  0001 C CNN
	1    1500 2225
	-1   0    0    -1  
$EndComp
$Comp
L Motor:Motor_DC M2
U 1 1 60970A38
P 8500 3575
F 0 "M2" H 8650 3625 50  0000 L CNN
F 1 "Motor_DC" H 8625 3500 50  0000 L CNN
F 2 "TerminalBlock_4UCON:TerminalBlock_4UCON_19963_02x3.5mm_Straight" H 8500 3485 50  0001 C CNN
F 3 "" H 8500 3485 50  0001 C CNN
	1    8500 3575
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_DC M3
U 1 1 60970AA0
P 8500 3975
F 0 "M3" H 8650 4025 50  0000 L CNN
F 1 "Motor_DC" H 8650 3950 50  0000 L CNN
F 2 "TerminalBlock_4UCON:TerminalBlock_4UCON_19963_02x3.5mm_Straight" H 8500 3885 50  0001 C CNN
F 3 "" H 8500 3885 50  0001 C CNN
	1    8500 3975
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_DC M4
U 1 1 60970AF7
P 8550 5175
F 0 "M4" H 8708 5171 50  0000 L CNN
F 1 "Motor_DC" H 8708 5080 50  0000 L CNN
F 2 "TerminalBlock_4UCON:TerminalBlock_4UCON_19963_02x3.5mm_Straight" H 8550 5085 50  0001 C CNN
F 3 "" H 8550 5085 50  0001 C CNN
	1    8550 5175
	1    0    0    -1  
$EndComp
$Comp
L regul:LF80_TO252 U6
U 1 1 60974A11
P 7200 1000
F 0 "U6" H 7200 1242 50  0000 C CNN
F 1 "LF60_TO252 6V regulator" H 7200 1151 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2" H 7200 1225 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/c4/0e/7e/2a/be/bc/4c/bd/CD00000546.pdf/files/CD00000546.pdf/jcr:content/translations/en.CD00000546.pdf" H 7200 950 50  0001 C CNN
	1    7200 1000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-DotStar-Wing-eagle-import:CAP_CERAMIC_0805 C2
U 1 1 60974FC4
P 7750 1200
F 0 "C2" H 7828 1296 50  0000 L CNN
F 1 "CAP_CERAMIC_0805" H 7828 1205 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7750 1200 50  0001 C CNN
F 3 "" H 7750 1200 50  0001 C CNN
	1    7750 1200
	1    0    0    -1  
$EndComp
$Comp
L Adafruit-DotStar-Wing-eagle-import:CAP_CERAMIC_0805 C1
U 1 1 6097501C
P 6700 1200
F 0 "C1" H 6778 1296 50  0000 L CNN
F 1 "CAP_CERAMIC_0805" H 6778 1205 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6700 1200 50  0001 C CNN
F 3 "" H 6700 1200 50  0001 C CNN
	1    6700 1200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack J5
U 1 1 609758AC
P 6050 1100
F 0 "J5" H 6128 1425 50  0000 C CNN
F 1 "Barrel_Jack" H 6128 1334 50  0000 C CNN
F 2 "TerminalBlock_4UCON:TerminalBlock_4UCON_19963_02x3.5mm_Straight" H 6100 1060 50  0001 C CNN
F 3 "~" H 6100 1060 50  0001 C CNN
	1    6050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1000 6900 1000
Wire Wire Line
	7500 1000 7750 1000
Wire Wire Line
	6700 1300 7200 1300
Wire Wire Line
	7200 1300 7750 1300
Connection ~ 7200 1300
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0103
U 1 1 60989069
P 7200 1450
F 0 "#PWR0103" H 7200 1200 50  0001 C CNN
F 1 "GND" V 7205 1322 50  0000 R CNN
F 2 "" H 7200 1450 50  0001 C CNN
F 3 "" H 7200 1450 50  0001 C CNN
	1    7200 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 1300 7200 1450
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0104
U 1 1 609896A2
P 6350 1300
F 0 "#PWR0104" H 6350 1050 50  0001 C CNN
F 1 "GND" V 6355 1172 50  0000 R CNN
F 2 "" H 6350 1300 50  0001 C CNN
F 3 "" H 6350 1300 50  0001 C CNN
	1    6350 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1200 6350 1300
Wire Wire Line
	6350 1000 6700 1000
Connection ~ 6700 1000
Text GLabel 8050 1000 2    50   Input ~ 0
V6V
Wire Wire Line
	7750 1000 8050 1000
Connection ~ 7750 1000
Text GLabel 3900 2925 2    50   Input ~ 0
V6V
Text GLabel 2600 4725 1    50   Input ~ 0
V3V
Text GLabel 3150 1425 1    50   Input ~ 0
V3V
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0105
U 1 1 609A036C
P 3000 2525
F 0 "#PWR0105" H 3000 2275 50  0001 C CNN
F 1 "GND" H 3005 2352 50  0000 C CNN
F 2 "" H 3000 2525 50  0001 C CNN
F 3 "" H 3000 2525 50  0001 C CNN
	1    3000 2525
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 1825 1400 1925
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0106
U 1 1 609A2D72
P 2425 1575
F 0 "#PWR0106" H 2425 1325 50  0001 C CNN
F 1 "GND" H 2430 1402 50  0000 C CNN
F 2 "" H 2425 1575 50  0001 C CNN
F 3 "" H 2425 1575 50  0001 C CNN
	1    2425 1575
	-1   0    0    -1  
$EndComp
Text GLabel 6700 700  2    50   Input ~ 0
VMOT
Wire Wire Line
	6700 700  6700 1000
Text GLabel 2750 1225 1    50   Input ~ 0
VMOT
Wire Wire Line
	2550 2125 2050 2125
$Comp
L device:R R1
U 1 1 609AE388
P 3200 2675
F 0 "R1" H 3270 2721 50  0000 L CNN
F 1 "R" H 3270 2630 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3130 2675 50  0001 C CNN
F 3 "" H 3200 2675 50  0001 C CNN
	1    3200 2675
	1    0    0    1   
$EndComp
$Comp
L teensy:Teensy3.2 U7
U 1 1 609B5A4C
P 5400 3575
F 0 "U7" H 5350 3725 60  0000 C CNN
F 1 "Teensy3.2" V 5550 3050 60  0000 C CNN
F 2 "multispeq:Teensy30_31_32_All_Pins" H 5475 5850 60  0000 C CNN
F 3 "" H 5400 2825 60  0000 C CNN
	1    5400 3575
	1    0    0    -1  
$EndComp
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0107
U 1 1 609B5E11
P 5000 6050
F 0 "#PWR0107" H 5000 5800 50  0001 C CNN
F 1 "GND" H 5005 5922 50  0000 R CNN
F 2 "" H 5000 6050 50  0001 C CNN
F 3 "" H 5000 6050 50  0001 C CNN
	1    5000 6050
	1    0    0    -1  
$EndComp
Text GLabel 5300 5950 3    50   Input ~ 0
V3V
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0108
U 1 1 609C5219
P 3825 3400
F 0 "#PWR0108" H 3825 3150 50  0001 C CNN
F 1 "GND" V 3830 3272 50  0000 R CNN
F 2 "" H 3825 3400 50  0001 C CNN
F 3 "" H 3825 3400 50  0001 C CNN
	1    3825 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3825 3225 3825 3400
Text GLabel 5250 1650 1    50   Input ~ 0
V6V
Wire Wire Line
	3200 5125 4400 5125
Wire Wire Line
	3200 5225 4400 5225
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0109
U 1 1 60A36EEE
P 3200 2825
F 0 "#PWR0109" H 3200 2575 50  0001 C CNN
F 1 "GND" V 3100 2850 50  0000 R CNN
F 2 "" H 3200 2825 50  0001 C CNN
F 3 "" H 3200 2825 50  0001 C CNN
	1    3200 2825
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 1725 3875 1725
Wire Wire Line
	3875 1725 3875 1825
Wire Wire Line
	3875 1825 3750 1825
Wire Wire Line
	3875 1825 3875 1925
Wire Wire Line
	3875 1925 3750 1925
Connection ~ 3875 1825
Text GLabel 4100 1825 2    50   Input ~ 0
V3V
$Comp
L Adafruit-DotStar-Wing-eagle-import:CAP_CERAMIC_0805 C3
U 1 1 60A3AC90
P 2425 1375
F 0 "C3" H 2347 1379 50  0000 R CNN
F 1 "CAP_CERAMIC_0805" H 2347 1470 50  0000 R CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2425 1375 50  0001 C CNN
F 3 "" H 2425 1375 50  0001 C CNN
	1    2425 1375
	1    0    0    1   
$EndComp
Wire Wire Line
	4100 1825 3875 1825
Wire Wire Line
	3750 2025 3875 2025
Wire Wire Line
	3875 2025 3875 2125
Wire Wire Line
	3875 2125 3750 2125
Wire Wire Line
	3825 2925 3900 2925
Wire Wire Line
	3750 2225 4400 2225
Wire Wire Line
	1900 2025 2550 2025
Wire Wire Line
	1900 2025 1900 2125
Wire Wire Line
	1900 2125 1800 2125
Wire Wire Line
	5000 5825 5000 6050
Wire Wire Line
	5250 1650 5250 1725
Text Notes 5150 325  0    50   ~ 0
Add limit switch\nAdd switch to indicate that the syringe body is correctly positioned under the CO2 sensor\nCheck Rx and Tx on CO2 sensor\nAdd a switch detect contact of the CO2 sensor and the syringe\nCheck pull-up resisters (Qwiik board has pull-ups. No need to add them)
Wire Wire Line
	4400 3025 3825 3025
Wire Wire Line
	4400 3125 3825 3125
Wire Wire Line
	2050 2325 1800 2325
Wire Wire Line
	2050 2125 2050 2325
Wire Wire Line
	4400 2325 3750 2325
Wire Wire Line
	2900 2525 3000 2525
Wire Wire Line
	2750 1225 2750 1275
Wire Wire Line
	2425 1275 2750 1275
Connection ~ 2750 1275
Wire Wire Line
	2750 1275 2750 1425
Connection ~ 3000 2525
$Comp
L multispec:Pololu_Stepper_Driver_A4988 U2
U 1 1 609706FB
P 3150 2375
F 0 "U2" H 3150 2775 60  0000 C CNN
F 1 "Pololu_Stepper_Driver_A4988" H 2950 3850 60  0000 C CNN
F 2 "multispeq:Pololu_Stepper_Driver_PCB" H 4350 2100 60  0000 C CNN
F 3 "" H 3150 2375 60  0000 C CNN
	1    3150 2375
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 5825 5000 5825
Connection ~ 5000 5825
Wire Wire Line
	5000 5825 5100 5825
Wire Wire Line
	5300 5825 5400 5825
Wire Wire Line
	5300 5825 5300 5950
Wire Wire Line
	6400 3675 7050 3675
Wire Wire Line
	6400 3875 7050 3875
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0102
U 1 1 60A12361
P 7650 4350
F 0 "#PWR0102" H 7650 4100 50  0001 C CNN
F 1 "GND" H 7875 4300 50  0000 R CNN
F 2 "" H 7650 4350 50  0001 C CNN
F 3 "" H 7650 4350 50  0001 C CNN
	1    7650 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4275 7550 4275
Wire Wire Line
	7550 4275 7650 4275
Connection ~ 7550 4275
Wire Wire Line
	7650 4275 7750 4275
Connection ~ 7650 4275
Wire Wire Line
	7750 4275 7850 4275
Connection ~ 7750 4275
Wire Wire Line
	7650 4350 7650 4275
Wire Wire Line
	8250 3475 8350 3475
Wire Wire Line
	8250 3675 8350 3675
Wire Wire Line
	8250 3875 8350 3875
Wire Wire Line
	8250 4075 8350 4075
Text GLabel 7950 3275 1    50   Input ~ 0
VMOT
Text GLabel 7650 3275 1    50   Input ~ 0
V3V
Wire Wire Line
	7300 3275 7400 3275
Wire Wire Line
	7400 3275 7650 3275
Connection ~ 7400 3275
$Comp
L soil-respiration-beta-rescue:GND-power #PWR0110
U 1 1 60A47FC9
P 7650 5950
F 0 "#PWR0110" H 7650 5700 50  0001 C CNN
F 1 "GND" H 7655 5822 50  0000 R CNN
F 2 "" H 7650 5950 50  0001 C CNN
F 3 "" H 7650 5950 50  0001 C CNN
	1    7650 5950
	1    0    0    -1  
$EndComp
Connection ~ 7650 5875
Wire Wire Line
	7650 5875 7650 5950
Text GLabel 7950 4875 1    50   Input ~ 0
VMOT
Text GLabel 7650 4875 1    50   Input ~ 0
V3V
Wire Wire Line
	7400 4875 7650 4875
Wire Wire Line
	7300 4875 7400 4875
Connection ~ 7400 4875
Wire Wire Line
	7450 5875 7550 5875
Wire Wire Line
	7550 5875 7650 5875
Connection ~ 7550 5875
Wire Wire Line
	7750 5875 7650 5875
Wire Wire Line
	7750 5875 7850 5875
Connection ~ 7750 5875
Connection ~ 5300 5825
Wire Wire Line
	6400 5275 7050 5275
Wire Wire Line
	8250 5075 8400 5075
Wire Wire Line
	8250 5275 8400 5275
Wire Wire Line
	7050 5375 6850 5375
Wire Wire Line
	6850 5375 6850 3775
Wire Wire Line
	6400 3775 6850 3775
Connection ~ 6850 3775
Wire Wire Line
	6850 3775 7050 3775
Wire Wire Line
	6400 2275 9175 2275
Wire Wire Line
	6400 2375 9175 2375
Text GLabel 9775 2075 1    50   Input ~ 0
V3V
Wire Wire Line
	1600 1925 2550 1925
Wire Wire Line
	2550 1825 1400 1825
$Comp
L soil-respiration-beta-rescue:Conn_01x04_Female-reordered J2
U 1 1 60AA9565
P 10750 3325
F 0 "J2" H 10778 3301 50  0000 L CNN
F 1 "Conn_01x04_Female-reordered" H 10778 3210 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 10750 3325 50  0001 C CNN
F 3 "" H 10750 3325 50  0001 C CNN
	1    10750 3325
	1    0    0    -1  
$EndComp
Wire Wire Line
	10375 3225 10550 3225
Wire Wire Line
	10550 3325 10375 3325
Wire Wire Line
	10550 3425 10375 3425
Wire Wire Line
	10550 3525 10375 3525
$Comp
L multispec:ITEAD_HC-05_fewerpin U1
U 1 1 609E57BF
P 2600 5775
F 0 "U1" H 3000 6975 60  0000 C CNN
F 1 "ITEAD_HC-05_fewerpin" H 2600 6819 60  0000 C CNN
F 2 "multispeq:HC_05_Bluetooth" H 2600 5775 60  0000 C CNN
F 3 "" H 2600 5775 60  0000 C CNN
	1    2600 5775
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2600 4825 2600 4725
Wire Wire Line
	2500 6725 2600 6725
Wire Wire Line
	2600 6725 2650 6725
Connection ~ 2600 6725
Wire Wire Line
	2700 6725 2800 6725
Connection ~ 2700 6725
Wire Wire Line
	2650 6850 2650 6725
Connection ~ 2650 6725
Wire Wire Line
	2650 6725 2700 6725
$Comp
L soil-respiration-beta-rescue:Conn_01x04_Female-reordered J3
U 1 1 60A1BE73
P 6600 2625
F 0 "J3" H 6628 2601 50  0000 L CNN
F 1 "Conn_01x04_Female-reordered" H 6628 2510 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 6600 2625 50  0001 C CNN
F 3 "" H 6600 2625 50  0001 C CNN
	1    6600 2625
	1    0    0    -1  
$EndComp
$EndSCHEMATC
